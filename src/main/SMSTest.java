package main;
import java.net.URI;
import java.net.URISyntaxException;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Call;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SMSTest 
{
  // Find your Account SID and Token at twilio.com/user/account
  public static final String ACCOUNT_SID = "AC4e7ccef6df1f6067440ebc8a6a027325";
  public static final String AUTH_TOKEN = "84710977784f1235f123303c45d57ab4";

  public static void main(String[] args) 
  {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

    Message message = Message.creator(
    	new PhoneNumber("+447903116538"),	//To number
        new PhoneNumber("+441290211890"), 	//From number Twillio generated number
        "First test SMS").create();			//Message

    System.out.println(message.getSid());
    
    try {
		Call call = Call.creator(
			    new PhoneNumber("+447903116538"),  //To number
			    new PhoneNumber("+441290211890"),  //From number Twillio generated number
			    new URI("http://twimlets.com/holdmusic?Bucket=com.twilio.music.ambient")).create();		//Link to music to play
	} catch (URISyntaxException e) 
    {
		System.out.println("Call exception");
		e.printStackTrace();
	}  
  }
}
